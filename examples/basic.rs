extern crate selfupdate;

use selfupdate::Updater;

fn main() {
    let mut updater = Updater::new(
        "1.0.0",
        "example",
        "https://dl.ishanjain.me/self_update",
        "https://dl.ishanjain.me/self_update",
        "self-update-test",
    );
    let result = updater.background_run();
    println!("{:?}", result);
    println!("{:?}", updater)
}
