extern crate clap;
extern crate libflate;
extern crate sha2;

use clap::{App, Arg};

const TARGET: &str = env!("TARGET");

fn main() {
    let matches = App::new("Selfupdate")
        .version("0.1")
        .author("Ishan Jain <ishanjain28@gmail.com>")
        .arg(
            Arg::with_name("target")
                .short("t")
                .long("target")
                .value_name("TARGET")
                .help("Target platform triplet")
                .takes_value(true),
        )
        .get_matches();

    let target = matches.value_of("TARGET").unwrap_or(TARGET);

    println!("{}", target);
}
