extern crate reqwest;
extern crate semver;
extern crate url;

use semver::Version;
use std::{env, fs, io, path};
use url::Url;

const TARGET: &str = env!("TARGET");

#[derive(Debug)]
pub struct Updater<'a> {
    pub current_version: &'a str,
    pub api_url: &'a str,
    pub cmd_name: &'a str,
    pub bin_url: &'a str,
    pub dir: &'a str,
    pub info: Info<'a>,
    update_available: bool,
}

#[derive(Debug)]
pub struct Info<'a> {
    version: &'a str,
    sha256: Vec<u8>,
}

impl<'a> Updater<'a> {
    pub fn new(
        current_version: &'a str,
        cmd_name: &'a str,
        api_url: &'a str,
        bin_url: &'a str,
        dir: &'a str,
    ) -> Updater<'a> {
        Updater {
            current_version,
            cmd_name,
            api_url,
            bin_url,
            dir,
            ..Default::default()
        }
    }

    pub fn background_run(&mut self) -> io::Result<()> {
        fs::create_dir_all(self.dir)?;

        if !self.should_update() {
            return Ok(());
        }
        println!("{}", TARGET);
        self.fetch_info();
        Ok(())
    }

    pub fn check_update(&mut self) -> bool {
        let raw_str = format!("{}/{}/{}.json", self.api_url, self.cmd_name, TARGET);

        let u = match Url::parse(&raw_str) {
            Ok(v) => v,
            Err(e) => panic!("error in parsing url {}", e),
        };

        let body = self.fetch(u);
        false
    }

    fn can_update(&self) -> bool {
        //TODO: Check for permission to write content to a file
        //TODO: Check acc. to semantic versioning scheme and verify if the binary should be updated
        false
    }

    fn fetch_info(&mut self) -> reqwest::Result<()> {
        let raw_str = format!("{}/{}/{}.json", self.api_url, self.cmd_name, TARGET);
        let u = match Url::parse(&raw_str) {
            Ok(v) => v,
            Err(e) => panic!("error in parsing url {}", e),
        };

        println!("{}", u);
        let body = self.fetch(u);

        println!("{:?}", body);

        Ok(())
    }

    fn fetch(&self, url: Url) -> reqwest::Response {
        match reqwest::get(url) {
            Ok(v) => v,
            Err(e) => panic!("{:?}", e),
        }
    }

    fn should_update(&self) -> bool {
        self.current_version != "dev"
            && Version::parse(self.current_version) < Version::parse(self.info.version)
    }
}

impl<'a> Default for Updater<'a> {
    fn default() -> Updater<'a> {
        Updater {
            current_version: "1.0.0",
            api_url: "",
            cmd_name: "",
            bin_url: "",
            dir: "",
            update_available: false,
            info: Info {
                version: "",
                sha256: vec![],
            },
        }
    }
}
